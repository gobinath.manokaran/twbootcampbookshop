package com.twbootcamp.bookshop;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShopController {
    @GetMapping("/application")
    public Book get() {
        return new Book("Book shop");
    }
}
