package com.twbootcamp.bookshop;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BookShopControllerTest {

    @Test
    public void shouldReturnBookInstance() {
        BookShopController bookShopController =new BookShopController();
        Book book = bookShopController.get();
        Assert.assertNotNull(book);
        Assert.assertEquals("Book shop",book.getName());
    }
}